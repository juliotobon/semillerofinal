package com.hbt.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PLATO")
@SequenceGenerator(name = "SEQ", sequenceName = "SEQ_COMUN", initialValue = 0, allocationSize = 1)

public class Plato {	

		@Id
		
		@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ")
		@Column(name = "PLATO_ID")
		private Long idPlato;

		@Column(name = "PLATO_NOMBRE")
		private int plato_nombre;

		@Column(name = "PRECIO")
		private String precio;
		
		@Column(name = "PLATO_DESCRIPCION")
		private String plato_descripcion;

		public Long getIdPlato() {
			return idPlato;
		}

		public void setIdPlato(Long idPlato) {
			this.idPlato = idPlato;
		}

		public int getPlato_nombre() {
			return plato_nombre;
		}

		public void setPlato_nombre(int plato_nombre) {
			this.plato_nombre = plato_nombre;
		}

		public String getPrecio() {
			return precio;
		}

		public void setPrecio(String precio) {
			this.precio = precio;
		}

		public String getPlato_descripcion() {
			return plato_descripcion;
		}

		public void setPlato_descripcion(String plato_descripcion) {
			this.plato_descripcion = plato_descripcion;
		}

		
	}

		


