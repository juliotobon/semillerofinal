package com.hbt.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BEBIDA")
public class Bebida {
	
	@Id		
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ")
	@Column(name = "BEBIDA_ID")
	private Long idBebida;

	@Column(name = "BEBIDA_NOMBRE")
	private int plato_nombre;

	@Column(name = "PRECIO")
	private String precio;

	public Long getIdBebida() {
		return idBebida;
	}

	public void setIdBebida(Long idBebida) {
		this.idBebida = idBebida;
	}

	public int getPlato_nombre() {
		return plato_nombre;
	}

	public void setPlato_nombre(int plato_nombre) {
		this.plato_nombre = plato_nombre;
	}

	public String getPrecio() {
		return precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}
	
	

}
