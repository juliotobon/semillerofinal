package com.hbt.semillero.dto;

public class PlatoDTO {
	
	private int plato_id; //identificador del plato
	private  String plato_nombre; //nombre del plato
	private String plato_descripcion;//descripcion del plato
	/**
	 * @return the plato_id
	 * metodo que obtiene el valor de la propiedad id_plato
	 */
	public int getPlato_id() {
		return plato_id;
	}
	/**
	 * @param plato_id metodo que asigna el valor id a la entidad plato
	 */
	public void setPlato_id(int plato_id) {
		this.plato_id = plato_id;
	}
	/**
	 * @return the plato_nombre 
	 * metodo que asigna el nombre a la entidad plato
	 */
	public String getPlato_nombre() {
		return plato_nombre;
	}
	/**
	 * @param plato_nombre the plato_nombre to set
	 * metodo que obtiene el valor nombre de la entidad plato
	 */
	public void setPlato_nombre(String plato_nombre) {
		this.plato_nombre = plato_nombre;
	}
	/**
	 * @return the plato_descripcion
	 * metodo que asigna el valor del campo descripcion plato
	 */
	public String getPlato_descripcion() {
		return plato_descripcion;
	}
	/**
	 * @param plato_descripcion the plato_descripcion to set
	 * metodo que retorna el valor descripcion_plato en la entidad plato
	 */
	public void setPlato_descripcion(String plato_descripcion) {
		this.plato_descripcion = plato_descripcion;
	}

	
	
}
