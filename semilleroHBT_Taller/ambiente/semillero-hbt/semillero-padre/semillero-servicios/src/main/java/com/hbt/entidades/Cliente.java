package com.hbt.entidades;

    import java.io.Serializable;
	import javax.persistence.Column;
	import javax.persistence.Entity;
	import javax.persistence.GeneratedValue;
	import javax.persistence.Id;
	import javax.persistence.Table;


	@Entity
	@Table(name = "CLIENTE")
	public class Cliente implements Serializable{
	

		private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(generator = "SEQ")
		@Column(name = "ID_CLIENTE")
		private Long idCliente;

		@Column(name = "NUM_ID")
		private String numeroIdentificacion;

		
		@Column(name = "NOMBRES")
		private String nombres;

		
		/**
		 * Obtiene el nombre completo de la persona.
		 * 
		 */
		protected String obtenerNombreCompleto() {
			return getNombres() + " " ;
		}


		public Long getIdCliente() {
			return idCliente;
		}


		public void setIdCliente(Long idCliente) {
			this.idCliente = idCliente;
		}


		public String getNumeroIdentificacion() {
			return numeroIdentificacion;
		}


		public void setNumeroIdentificacion(String numeroIdentificacion) {
			this.numeroIdentificacion = numeroIdentificacion;
		}


		public String getNombres() {
			return nombres;
		}


		public void setNombres(String nombres) {
			this.nombres = nombres;
		}
	
	}


