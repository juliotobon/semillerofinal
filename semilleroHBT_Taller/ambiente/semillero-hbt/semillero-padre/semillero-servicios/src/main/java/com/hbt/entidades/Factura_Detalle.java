package com.hbt.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "FACTURA_DETALLE")
public class Factura_Detalle {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ")	
	@Column(name = "DETALLE_ID")
	private Long detalle_id;

	@Column(name = "PLATO_NOMBRE")
	private int plato_nombre;

	@Column(name = "PRECIO")
	private String precio;
	
	@Column(name = "PLATO_DESCRIPCION")
	private String plato_descripcion;
	
	@OneToOne
	@JoinColumn(name = "FACTURA")
	private Factura factura;
	
	@ManyToOne
	@JoinColumn(name = "PLATO")
	private Plato plato;
	
	@ManyToOne
	@JoinColumn(name = "BEBIDA")
	private Bebida bebida;
	
	

}
