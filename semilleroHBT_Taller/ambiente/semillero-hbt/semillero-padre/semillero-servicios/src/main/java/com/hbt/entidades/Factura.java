package com.hbt.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "FACTURA")
public class Factura {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ")	
	@Column(name = "FACTURA_ID")
	private Long factura_id;

	@Column(name = "PLATO_NOMBRE")
	private int plato_nombre;

	@Column(name = "PRECIO")
	private String precio;
	
	@Column(name = "PLATO_DESCRIPCION")
	private String plato_descripcion;
	
	@ManyToOne
	@JoinColumn(name = "CLIENTE")
	private Cliente cliente;

	public Long getFactura_id() {
		return factura_id;
	}

	public void setFactura_id(Long factura_id) {
		this.factura_id = factura_id;
	}

	public int getPlato_nombre() {
		return plato_nombre;
	}

	public void setPlato_nombre(int plato_nombre) {
		this.plato_nombre = plato_nombre;
	}

	public String getPrecio() {
		return precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}

	public String getPlato_descripcion() {
		return plato_descripcion;
	}

	public void setPlato_descripcion(String plato_descripcion) {
		this.plato_descripcion = plato_descripcion;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

}
