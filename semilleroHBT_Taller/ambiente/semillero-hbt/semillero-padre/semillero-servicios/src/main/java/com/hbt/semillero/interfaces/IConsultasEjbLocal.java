
package com.hbt.semillero.interfaces;

import java.util.List;
import com.hbt.semillero.dto.PlatoDTO;
import com.hbt.semillero.dto.BebidaDTO;
import com.hbt.semillero.dto.ClienteDTO;
import com.hbt.semillero.dto.FacturaDTO;


import com.hbt.semillero.dto.ResultadoDTO;

/**
 * @author Julio Cesar
 *
 */
public interface IConsultasEjbLocal {
	
	/**
	 * Consulta los platos existentes.
	 *
	 * 
	 * @return
	 */
	public <PlatoDTO> List<PlatoDTO> consultarPlatosExistentes();

	/**
	 * Consulta los Bebidas existentes.
	 * 
	 */
	public List<BebidaDTO> consultarBebidasExistentes();

	/**
	 * Consulta las personas que cumplan con los criterios ingresados.
	 * 
	 * @param cliente_id
	 * @param cliente_nombre
	 * 
	 */
	public List<ClienteDTO> consultarClientes(int cliente_id , String cliente_nombre);
	
	/**
	 * Consulta las facturas dentro del sistema con  los criterios ingresados 
	 * 
	 * @param cliente_id
	 * @param factura_id
	 * 
	 */
	public List<FacturaDTO> consultarFactura(int cliente_id , int factura_id);
	/**
	 * Crea los clientes  dentro del sistema.
	 * 
	 * @param ClienteDTO
	 * @return
	 */
	public ResultadoDTO crearCliente(ClienteDTO clienteDTO);
	
	/**
	 * 
	 * @param facturaDTO
	 * @return
	 * crea las facturas despues de la venta 
	 */
	
	public ResultadoDTO crearFactura(FacturaDTO facturaDTO);
	 

}



