package com.hbt.semillero.servicios.ejb;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import com.hbt.semillero.interfaces.IConsultasEjbLocal;



public class ConsultasEJB {
	

/**
 * EJB de consultas
 */
@Stateless
public class ConsultasEJB implements IConsultasEjbLocal {

	@PersistenceContext
	private EntityManager em;

	/**
	 * {@link com.hbt.semillero.servicios.interfaces.IConsultasEjbLocal#consultarPlatosExistentes()}
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<PlatoDTO> consultarPlatosExistentes() {
		
		List<Plato> plato = em.createQuery("Select plato from Plato plato").getResultList();
		List<PlatoDTO> platosRetorno = new ArrayList<>();
		
			for (Plato plato : platos) {
			PlatoDTO platoDto = construirPlatoDTO(plato);
			platosRetorno.add(platoDto);
		}
		return platosRetorno;
	}

    @Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<BebidaDTO> consultarBebidasExistentes() {
		List<Bebida> plato = em.createQuery("Select bebida from Bebida bebida").getResultList();
		List<BebidaDTO> bebidasRetorno = new ArrayList<>();
		for (Bebida bebida : bebidas) {
			BebidaDTO bebidaDto = construirBebidaDTO(bebida);
			platosRetorno.add(bebidaDto);
		}
		return bebidasRetorno;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ClienteDTO> consultarClientes(Int idCliente ) {
		StringBuilder consulta = new StringBuilder("Select cli FROM Cliente cli WHERE 1=1 ");
		Map<String,Object> parametros = new HashMap<>();
		
		if (idCliente != null) {
			consulta.append("and per.numeroIdentificacion =:idCliente");
			parametros.put("idCliente", idCliente);
		}
		Query query = em.createQuery(consulta.toString());

		for (Entry<String, Object> entry : parametros.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue()).getResultList();
		}

		List<Cliente> clientes = query.getResultList();
		List<ClienteDTO> clientesRetorno = new ArrayList<>();
		for (Cliente cliente : clientes) {
			ClienteDTO personaDTO = new ClienteDTO();
			ClienteDTO.setNombre(Cliente.getNombre());
			ClienteDTO.setNumeroIdentificacion(persona.getNumeroIdentificacion());
			

			clientesRetorno.add(clienteDTO);
		}
		return clientesRetorno;
	}

/**
 * 
 * @param personaDTO
 * @return
 * creacion de clientes en la base de datos 
 */
		@Override
		@TransactionAttribute(TransactionAttributeType.REQUIRED)
		public ResultadoDTO crearPersona(PersonaDTO personaDTO) {
			try {
				Cliente cliente = asignarAtributosBasicos(clienteDTO);
				em.persist(cliente);
				
					Cliente cliente = new Cliente();
					cliente.setCliente(cliente);
					em.persist(cliente);
				}
				
			catch (Exception e) {
				return new ResultadoDTO(false, e.getMessage());
			}

			return new ResultadoDTO(true, "Creado de forma exitosa");
		}
		
		
	/**
	 * Asigna los atributos básicos del cliente
	 * 
	 * @param cliente
	 * @param clienteDTO
	 */
	private Cliente asignarAtributosBasicos(ClienteDTO clienteDTO) {
		Cliente cliente = new Cliente();
		cliente.setNombre(clienteDTO.getNombre());
		cliente.setNumeroIdentificacion(personaDTO.getNumeroIdentificacion());
		return cliente;
	}

}
}
