package com.hbt.semillero.dto;

public class ClienteDTO {

	private int cliente_id;//identificador unico para cada cliente
	private String cliente_nombre; //nombre del cliente
	/**
	 * @return the cliente_id
	 * Método que obtiene el valor de la propiedad id cliente
	 */
	public int getCliente_id() {
		return cliente_id;
	}
	/**
	 * @param cliente_id the cliente_id to set
	 * Método que asigna el valor de la propiedad id cliente  
	 */
	public void setCliente_id(int cliente_id) {
		this.cliente_id = cliente_id;
	}
	/**
	 * @return the cliente_nombre
	 * Método que obtiene el valor de la propiedad cliente_nombre
	 */
	public String getCliente_nombre() {
		return cliente_nombre;
	}
	/**
	 * @param cliente_nombre the cliente_nombre to set
	 * Método que asigna el valor de la propiedad cliente_nombre
	 */
	public void setCliente_nombre(String cliente_nombre) {
		this.cliente_nombre = cliente_nombre;
	}
	
	
}
