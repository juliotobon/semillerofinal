package com.hbt.semillero.dto;

public class BebidaDTO {
	
	private int bebida_id; // identificador de la bebida
    private String	bebida_nombre; //nombre de la bebida
	private int precio;//precio de la bebida
	/**
	 * @return the bebida_id
	 * obtiene el valor del campo id de la tabla bebida
	 */
	public int getBebida_id() {
		return bebida_id;
	}
	/**
	 * @param bebida_id the bebida_id to set
	 * asigna el id en la entidad bebida
	 */
	public void setBebida_id(int bebida_id) {
		this.bebida_id = bebida_id;
	}
	/**
	 * @return the bebida_nombre
	 * obtiene el nombre de la bebida 
	 */
	public String getBebida_nombre() {
		return bebida_nombre;
	}
	/**
	 * @param bebida_nombre the bebida_nombre to set
	 * asigna el valor del nombre en la tabla bebida
	 */
	public void setBebida_nombre(String bebida_nombre) {
		this.bebida_nombre = bebida_nombre;
	}
	/**
	 * @return the precio
	 * obtiene el valor del precio en la entidad bebida
	 */
	public int getPrecio() {
		return precio;
	}
	/**
	 * @param precio the precio to set
	 * asigna el precio en la entidad bebida 
	 */
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	
	
}
