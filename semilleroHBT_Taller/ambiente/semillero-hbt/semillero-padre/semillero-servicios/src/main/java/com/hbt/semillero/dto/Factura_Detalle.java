package com.hbt.semillero.dto;

public class Factura_Detalle {
	
	 private int detalle_id; //detalle de la venta 
	 private int factura_id;//identificador de la factura de la venta
	 private int plato_id;//identificador del plato
	 private int bebida_id;//identificador de la bebida
	/**
	 * @return the detalle_id
	 * Método que obtiene el valor de la propiedad detalle_id
	 */
	public int getDetalle_id() {
		return detalle_id;
	}
	/**
	 * @param detalle_id the detalle_id to set
	 * Método que asigna el valor de la propiedad detalle_id
	 */
	public void setDetalle_id(int detalle_id) {
		this.detalle_id = detalle_id;
	}
	/**
	 * @return the factura_id
	 * Método que obtiene el valor de la propiedad factura_id
	 */
	public int getFactura_id() {
		return factura_id;
	}
	/**
	 * @param factura_id the factura_id to set
	 * Método que asigna el valor de la propiedad factura_id
	 */
	public void setFactura_id(int factura_id) {
		this.factura_id = factura_id;
	}
	/**
	 * @return the plato_id
	 * Método que obtiene el valor de la propiedad plato_id
	 */
	public int getPlato_id() {
		return plato_id;
	}
	/**
	 * @param plato_id the plato_id to set
	 * Método que asigna el valor de la propiedad plato_id
	 * 
	 */
	public void setPlato_id(int plato_id) {
		this.plato_id = plato_id;
	}
	/**
	 * @return the bebida_id}
	 * Método que obtiene el valor de la propiedad bebida_id
	 */
	public int getBebida_id() {
		return bebida_id;
	}
	/**
	 * @param bebida_id the bebida_id to set
	 * Método que asigna el valor de la propiedad bebida_id
	 */
	public void setBebida_id(int bebida_id) {
		this.bebida_id = bebida_id;
	}
	 
	 
	 

}

