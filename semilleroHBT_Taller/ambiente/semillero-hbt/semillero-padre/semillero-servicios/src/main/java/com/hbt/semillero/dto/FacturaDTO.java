package com.hbt.semillero.dto;

public class FacturaDTO {
	
	private int factura_id;//identificador de la factura
	private int cliente_id;//identificador del cliente
	private int iva; //impuesto a la compra 19%
	private int total;//total de la compra
	/**
	 * @return the factura_id
	 * Método que obtiene el valor de la propiedad factura_id
	 */
	public int getFactura_id() {
		return factura_id;
	}
	/**
	 * @param factura_id the factura_id to set
	 * Método que asigna el valor de la propiedad id_factura
	 */
	public void setFactura_id(int factura_id) {
		this.factura_id = factura_id;
	}
	/**
	 * @return the cliente_id
	 * Método que obtiene el valor de la propiedad cliente_id
	 */
	public int getCliente_id() {
		return cliente_id;
	}
	/**
	 * @param cliente_id the cliente_id to set
	 * Método que asigna el valor de la propiedad id_cliente
	 */
	public void setCliente_id(int cliente_id) {
		this.cliente_id = cliente_id;
	}
	/**
	 * @return the iva
	 * Método que obtiene el valor de la propiedad iva   
	 */ 
	public int getIva() {
		return iva;
	}
	/**
	 * @param iva the iva to set
	 * Método que asigna el valor de la propiedad iva
	 */
	public void setIva(int iva) {
		this.iva = iva;
	}
	/**
	 * @return the total
	 * Método que obtiene el valor de la propiedad total
	 */ 
	public int getTotal() {
		return total;
	}
	/**
	 * @param total the total to set
	 * Método que asigna el valor de la propiedad total
	 */
	public void setTotal(int total) {
		this.total = total;
	}
	
	

}
